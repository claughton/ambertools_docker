from setuptools import setup, find_packages
setup(
    name = 'amberdocker',
    version = '18-tools',
    packages = find_packages(),
    scripts = [
        'amber/scripts/sander',
        'amber/scripts/amber-shell',
    ],
)
