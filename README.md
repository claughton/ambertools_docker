A Dockerized version of AmberTools 18.
=================================

This repo provides Ambertools 18 in a dockerized way. Instead of 
installing the software, you install "shims" with the same names as the key 
Amber executables that, when run, download (if neccessary) and then launch 
a Docker container to actually execute the command.

Installation:
-------------
The easy way:
```
pip install git+https://bitbucket.org/claughton/amber_docker.git@v18-tools
```

or:
```
% git clone https://bitbucker.org/claughton/amber_docker.git
% git checkout v18-tools
% cd amber_docker/ambertools
% make install
```

To remove:
```
% make uninstall
```

Usage:
------

The installation adds a number of scripts to /usr/local/bin (or wherever):

* sander: provides a shim the Amber 'sander' command.
* amber-shell: drops you into a bash shell where you can use the full set of
  more interactive Amber tools (e.g. tleap, cpptraj, etc.)

In the example folder is a short md job you can use to test your installation.

Rebuilding the Docker image:
----------------------------

If you want to build your own copy of the image) you will need to
obtain your own copy of the compressed tarball for AmberTools (AmberTools18.tar.bz2. 
Place this file in the ./data directory. Next edit the Makefile to set your own Docker username, then:
```
% make build
% make publish
```
