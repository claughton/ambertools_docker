PREFIX ?= /usr/local
VERSION = 18
DOCKERID = claughton

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/sander $(DESTDIR)$(PREFIX)/bin/sander
	install -m 0755 scripts/amber-shell $(DESTDIR)$(PREFIX)/bin/amber-shell

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/sander
	@$(RM) $(DESTDIR)$(PREFIX)/bin/amber-shell
	@docker rmi $(DOCKERID)/ambertools:$(VERSION)
	@docker rmi $(DOCKERID)/ambertools:latest

build: Dockerfile
	@docker build -t $(DOCKERID)/ambertools:$(VERSION) . \
	&& docker tag $(DOCKERID)/ambertools:$(VERSION) $(DOCKERID)/ambertools:latest

publish: build
	@docker push $(DOCKERID)/ambertools:$(VERSION) \
	&& docker push $(DOCKERID)/ambertools:latest

.PHONY: all install uninstall build publish
